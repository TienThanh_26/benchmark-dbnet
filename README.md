## How to Run

  ```
  mkdir build
  cd build
  cmake ..
  make
  ./dbnet -s "Path to engine file" "Path to images test folder"
  ./dbnet -d /workspace/db_onnx/db_resnet18.onnx_b1_gpu0_fp32.engine /workspace/samples/img_test_dbnet/
  ```


