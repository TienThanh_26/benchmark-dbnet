#include <iostream>
#include "cuda_runtime_api.h"
#include "logging.h"
#include "common.hpp"
#include <math.h>
#include "utils.h"
#include <iterator>
#include <ostream>
#include <string>
#define DEVICE 0  // GPU id
#define EXPANDRATIO 1.5
#define BOX_MINI_SIZE 5
#define SCORE_THRESHOLD 0.3
#define BOX_THRESHOLD 0.7
#define BATCH_SIZE 1

static const int SHORT_INPUT = 224;
static const int MAX_INPUT_SIZE = 512; // 32x
static const int MIN_INPUT_SIZE = 160;
static const int OPT_INPUT_W = 224;
static const int OPT_INPUT_H = 224;

const char* INPUT_BLOB_NAME = "images";
const char* OUTPUT_BLOB_NAME = "output";
static Logger gLogger;


float paddimg(cv::Mat& In_Out_img, int shortsize = 224) {
    int w = In_Out_img.cols;
    int h = In_Out_img.rows;
    float scale = 1.f;
    if (w < h) {
        scale = (float)shortsize / w;
        h = scale * h;
        w = shortsize;
    }
    else {
        scale = (float)shortsize / h;
        w = scale * w;
        h = shortsize;
    }

    if (h % 32 != 0) {
        h = (h / 32 + 1) * 32;
    }
    if (w % 32 != 0) {
        w = (w / 32 + 1) * 32;
    }

    cv::resize(In_Out_img, In_Out_img, cv::Size(shortsize, shortsize));
    return scale;
}

void doInference(IExecutionContext& context, cudaStream_t& stream, void **buffers, float* input, float* output, int h_scale, int w_scale) {

    // DMA input batch data to device, infer on the batch asynchronously, and DMA output back to host
    CUDA_CHECK(cudaMemcpyAsync(buffers[0], input, BATCH_SIZE * 3 * h_scale * w_scale * sizeof(float), cudaMemcpyHostToDevice, stream));
    context.enqueue(BATCH_SIZE, buffers, stream, nullptr);
    CUDA_CHECK(cudaMemcpyAsync(output, buffers[1], BATCH_SIZE * h_scale * w_scale * 2 * sizeof(float), cudaMemcpyDeviceToHost, stream));
    cudaStreamSynchronize(stream);

}


int main(int argc, char** argv) {
    cudaSetDevice(DEVICE);
    char *trtModelStream{ nullptr };
    size_t size{ 0 };

    auto img_path = argv[2];

    std::ifstream file(std::string(argv[1]), std::ios::binary);
    // std::ifstream file("/workspace/db_onnx/db_resnet18.onnx_b1_gpu0_fp32.engine", std::ios::binary);
    // std::ifstream file("/workspace/code/engine/dbnet/build_b32/DBNet.engine", std::ios::binary);
    if (file.good()) {
        file.seekg(0, file.end);
        size = file.tellg();
        file.seekg(0, file.beg);
        trtModelStream = new char[size];
        assert(trtModelStream);
        file.read(trtModelStream, size);
        file.close();
    }

    static float data[BATCH_SIZE * 3 * 224 * 224];
    static float prob[BATCH_SIZE * 224 * 224 * 2];
    // prepare input data ---------------------------
    IRuntime* runtime = createInferRuntime(gLogger);
    assert(runtime != nullptr);
    ICudaEngine* engine = runtime->deserializeCudaEngine(trtModelStream, size);
    assert(engine != nullptr);
    IExecutionContext* context = engine->createExecutionContext();
    assert(context != nullptr);
    delete[] trtModelStream;

    std::cout<<"Engine good."<<std::endl;

    // Pointers to input and output device buffers to pass to engine.
    // Engine requires exactly IEngine::getNbBindings() number of buffers.
    assert(engine->getNbBindings() == 2);
    void* buffers[2];

    // In order to bind the buffers, we need to know the names of the input and output tensors.
    // Note that indices are guaranteed to be less than IEngine::getNbBindings()
    const int inputIndex = engine->getBindingIndex(INPUT_BLOB_NAME);
    const int outputIndex = engine->getBindingIndex(OUTPUT_BLOB_NAME);
    context->setBindingDimensions(inputIndex, Dims4(1, 3, 224, 224));
    assert(inputIndex == 0);
    assert(outputIndex == 1);
    // Create GPU buffers on device
    CUDA_CHECK(cudaMalloc(&buffers[inputIndex],BATCH_SIZE * 3 * 224 * 224 * sizeof(float)));
    CUDA_CHECK(cudaMalloc(&buffers[outputIndex],BATCH_SIZE * 2 * 224 * 224 * sizeof(float)));

    // Create stream
    cudaStream_t stream;
    CUDA_CHECK(cudaStreamCreate(&stream));


    std::vector<std::string> file_names;
    if (read_files_in_dir(img_path, file_names) < 0) {
        std::cout << "read_files_in_dir failed." << std::endl;
        return -1;
    }

    // icdar2015.yaml Hyperparameter
    std::vector<float> mean_value{ 0.406, 0.456, 0.485 };  // BGR
    std::vector<float> std_value{ 0.225, 0.224, 0.229 };

    int fcount = 0;
    std::cout << (int)file_names.size() <<std::endl;
    // auto start_time = high_resolution_clock::now();
    for (int f = 0; f < (int)file_names.size(); f++) {
        fcount++;
        if (fcount < BATCH_SIZE && f + 1 != (int)file_names.size()) continue;
 
        for (int b = 0; b < fcount; b++) {
            cv::Mat pr_img = cv::imread(std::string(img_path) + "/" + file_names[f - fcount + 1 + b]);
            if (pr_img.empty()) continue;
            float scale = paddimg(pr_img, SHORT_INPUT);
            if (pr_img.cols < MIN_INPUT_SIZE || pr_img.rows < MIN_INPUT_SIZE) continue;
            
            int i = 0;
            for (int row = 0; row < pr_img.rows; ++row) {
                uchar* uc_pixel = pr_img.data + row * pr_img.step;
                for (int col = 0; col < pr_img.cols; ++col) {
                    data[b * 3 * pr_img.rows * pr_img.cols + i] = (uc_pixel[2] / 255.0 - mean_value[2]) / std_value[2];
                    data[b * 3 * pr_img.rows * pr_img.cols + i + pr_img.rows * pr_img.cols] = (uc_pixel[1] / 255.0 - mean_value[1]) / std_value[1];
                    data[b * 3 * pr_img.rows * pr_img.cols + i + 2 * pr_img.rows * pr_img.cols] = (uc_pixel[0] / 255.0 - mean_value[0]) / std_value[0];
                    uc_pixel += 3;
                    ++i;
                }
            }
        }      

        // Run inference
        auto start = std::chrono::system_clock::now();
        doInference(*context, stream, buffers, data, prob, 224, 224);
        auto end = std::chrono::system_clock::now();
        std::cout << "Detect time:"<< std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << "microseconds" << std::endl;

        fcount = 0;
    }

    // auto stop_time = high_resolution_clock::now();
    // auto duration = duration_cast<microseconds>(stop_time - start_time);
    // std::cout << "Time taken by function: "<< duration.count() << " microseconds" << std::endl;

    // // Release stream and buffers
    cudaStreamDestroy(stream);
    CUDA_CHECK(cudaFree(buffers[inputIndex]));
    CUDA_CHECK(cudaFree(buffers[outputIndex]));

    // Destroy the engine
    context->destroy();
    engine->destroy();
    runtime->destroy();

    return 0;
}